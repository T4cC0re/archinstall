#!/usr/bin/env bash

for lib in lib/*; do
    source ${lib}
done

[ -n "$AI_USERNAME" ] || read -p "Enter username to create: " AI_USERNAME </dev/tty
echo

[ -n "$AI_USERPASS" ] || read -s -p "Enter password for user '${AI_USERNAME}': " AI_USERPASS </dev/tty
echo

[ -n "$ROOTPW" ] || [ -n "$NOROOTPW" ] || read -sp "Enter rootpw to set (empty to disable):" ROOTPW </dev/tty

echo
echo "Disks:"
lsblk

while ! diskExists; do : ;done
# $INSTALLDEVICE is now set

[ -n "$CRYPTPW" ] || [ -n "$NOCRYPTPW" ] || [ -n "$GENCRYPTPW" ] || read -sp "Enter password for FDE (empty to disable):" CRYPTPW </dev/tty

# Time. Tick-tock
timedatectl set-ntp true
hwclock --systohc

partition
# $BOOTDEVICE = ${INSTALLDEVICE}1 = /boot
# $ROOTDEVICE = ${INSTALLDEVICE}2 = / or encrypted LUKS

pacman -Sy --noconfirm pwgen

setupCryptDevice
# If $CRYPTPW is set, see below, otherwise no changes
# $ROOTDEVICE = /dev/mapper/crypt
# $CRYPTDEVICE = old $ROOTDEVICE

mkfs.fat -F 32 "${BOOTDEVICE}"
mkfs.btrfs -f "${ROOTDEVICE}"

mount ${ROOTDEVICE} /mnt
mkdir -p /mnt/boot
mount ${BOOTDEVICE} /mnt/boot

# install base system
pacstrap /mnt base base-devel btrfs-progs sudo git upx ${PKGS}

# generate fstab
genfstab -U /mnt >> /mnt/etc/fstab

# prepare arch-chroot
cp -rvp /tmp/archinstall /mnt/opt
cat > /mnt/opt/archinstall/.env <<EOF
INSTALLDEVICE="${INSTALLDEVICE}"
BOOTDEVICE="${BOOTDEVICE}"
ROOTDEVICE="${ROOTDEVICE}"
CRYPTDEVICE="${CRYPTDEVICE}"
ROOTPW="${ROOTPW}"
NOROOTPW="${NOROOTPW}"
CRYPTPW="${CRYPTPW}"
NOCRYPTPW="${NOCRYPTPW}"
TIMEZONE="${TIMEZONE}"
LOCALE="${LOCALE}"
HOSTNAME="${HOSTNAME}"
GENCRYPTPW="${GENCRYPTPW}"
VAULT_ADDR="${VAULT_ADDR}"
VAULT_TOKEN="${VAULT_TOKEN}"
ETH="${ETH}"
KERNELPARAMS="${KERNELPARAMS}"
AI_USERNAME="${AI_USERNAME}"
EOF

# Execute chroot.sh in arch-chroot to finish install
arch-chroot /mnt /opt/archinstall/chroot.sh
