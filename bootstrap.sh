#!/usr/bin/env bash

set -ea

echo "ArchInstall bootstrap"
echo

if [ -n "$DEBUG" ]; then
  echo 'enabling set -x'
  echo 'BEWARE! THIS MAY LEAK PASSWORDS!'
  set -x
fi

if ! [ -d /sys/firmware/efi/efivars ]; then
  echo 'Not booted in UEFI mode. Not supported.'
  return 1
fi

BRANCH="${BRANCH:-master}"

# Are we local at @t4cc0re's home? If so, prefer his local mirror.
if ping -c1 -W1 mirror.t4cc0.re &> /dev/null; then
  cat > /etc/pacman.d/mirrorlist <<\EOF
## T4cC0re Arch Linux repository mirrorlist

Server = https://mirror.t4cc0.re/archlinux/$repo/os/$arch
Server = https://mirror.leaseweb.net/archlinux/$repo/os/$arch

EOF
fi

pacman -Sy --noconfirm git

rm -rf /tmp/archinstall
git clone --depth=1 https://gitlab.com/T4cC0re/archinstall.git /tmp/archinstall
cd /tmp/archinstall
git checkout "${BRANCH}"

source install.sh
