#!/usr/bin/env

mkdir .ssh
curl -sSL https://gitlab.com/${1}.keys >> .ssh/authorized_keys
pacman -Sy --noconfirm openssh
systemctl enable --now sshd
