#!/usr/bin/env bash

diskExists () {
    while [ -z "$DISK" ]; do
        echo
        read -p "Enter disk (not partition) to use (e.g. sda or nvme0n1): " DISK </dev/tty
    done
    if lsblk "/dev/${DISK}" &> /dev/null; then
        INSTALLDEVICE="/dev/${DISK}"
        return 0
    else
        DISK=
        return 1
    fi
}