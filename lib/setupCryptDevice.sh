#!/usr/bin/env bash

setupCryptDevice () {
    if [ -z "$CRYPTPW" ]; then
        if [ -n "$GENCRYPTPW" ]; then
            CRYPTPW=$(pwgen -sB 48 1)
            echo "Generated password for FDE:"
            echo "${CRYPTPW}"
        else
            echo "No CRYPTPW set. Not enabling FDE"
            return
        fi
    fi

    CRYPTDEVICE="${ROOTDEVICE}"
    ROOTDEVICE=/dev/mapper/crypt
    # Clean any rests so luksFormat does not complain
    dd if=/dev/zero of="${CRYPTDEVICE}" bs=1M count=2
    # While urandom is enough, this is to be expected to be run on a server system where entropy might be limited. So we use /dev/random here
    echo -n "$CRYPTPW" | cryptsetup -v --type luks --cipher aes-xts-plain64 --key-size 256 --hash sha256 --iter-time 5000 --use-random luksFormat "${CRYPTDEVICE}" -
    echo -n "$CRYPTPW" | sudo cryptsetup luksOpen "${CRYPTDEVICE}" crypt -d -
    ls -lsa /dev/mapper
}
