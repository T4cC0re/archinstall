#!/usr/bin/env bash

partition () {
    umount /mnt/boot || true
    umount /mnt || true
    cryptsetup close /dev/mapper/crypt || true
    mount | grep "${INSTALLDEVICE}" | awk '{print $1}' | xargs -rn1 umount
    sudo dd if=/dev/zero "of=${INSTALLDEVICE}" bs=512 count=2
    partprobe || true

    fdisk "${INSTALLDEVICE}" <<EOF
p
g
n
1

+512M
t
1
n
2


p
w
EOF
    if [[ $INSTALLDEVICE == *"/dev/nvme"* ]]; then
      BOOTDEVICE=${INSTALLDEVICE}p1
      ROOTDEVICE=${INSTALLDEVICE}p2
    else
      BOOTDEVICE=${INSTALLDEVICE}1
      ROOTDEVICE=${INSTALLDEVICE}2
    fi
    partprobe || true
}