# archinstall

[WIP] Script to install arch with just a curl

From an archiso execute
```
curl -sSL https://gitlab.com/T4cC0re/archinstall/-/raw/master/bootstrap.sh | [config] bash
```

$CI_PAGES_URL $CI_COMMIT_BRANCH

Configuration variables
- `AI_USERNAME` sets the username of the primary (non-root, but sudo'd) user.
- `AI_USERPASS` sets the user password. If not provided, you will be asked.s
- `DISK` sets the device (not partition, w/o `/dev/`-prefix) to install on. If not provided you will be asked
- `ROOTPW` sets the root password to the given string. If not provided you will be asked
- `NOROOTPW` Does not ask you and set a blank root password
- `CRYPTPW` set the passphrase for the FDE ti the given string. If not provided you will be asked
- `NOCRYPTPW` Does not ask for a CRYPTPW and disable FDE
- `GENCRYPTPW` Does not ask for a CRYPTPW but generate a random one. It will be provided to you at the end of the installation.
- `TIMEZONE` Sets a timezone (default UTC)
- `LOCALE` Sets a locale (default en_US.UTF-8)
- `HOSTNAME` Sets a hostname (default archinstall)
- `VAULT_ADDR` Set this to be able to use remote unlock
- `VAULT_TOKEN` ditto
- `ETH` Set this to the primary interface to use for networking. DHCP will be enabled there. (required)
- `KERNELPARAMS` Set anny supplemental kernel parameters
