#!/usr/bin/env bash

set -ea

error () {
    echo $@
    echo "edit /mnt/opt/archinstall/.env to adapt then execute"
    echo "arch-chroot /mnt /opt/archinstall/chroot.sh"
    echo "to retry"
    exit 1
}

trap error EXIT

cd /opt/archinstall

source .env

if [ -n "$DEBUG" ]; then
    set -x
fi

# TimeZone config, UTC is default
TIMEZONE="${TIMEZONE:-UTC}"
if ! [ -f "/usr/share/zoneinfo/${TIMEZONE}" ]; then
    error "timezone ${TIMEZONE} does not exist"
fi
ln -sf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime

# LOCALE config, en_US.UTF-8 is default
LOCALE="${LOCALE:-en_US.UTF-8}"
if ! egrep "^#?${LOCALE} UTF-8" /etc/locale.gen; then
    error "locale ${LOCALE} does not exist"
fi
echo "${LOCALE} UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=${LOCALE}" > /etc/locale.conf

# Hostname config
HOSTNAME="${HOSTNAME:-archinstall}"
echo "${HOSTNAME}" > /etc/hostname

# Setup users
useradd -mU "${AI_USERNAME}"
useradd -mU pkgbuild
echo "pkgbuild ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/pkgbuild
echo "${AI_USERNAME} ALL=(ALL) NOPASSWD: ALL" > "/etc/sudoers.d/${AI_USERNAME}"
[ -z "$ROOTPW" ] || (echo "root:${ROOTPW}" | chpasswd)
echo "${AI_USERNAME}:${AI_USERPASS}" | chpasswd
# Deploy SSH Keys for GitLab users
mkdir /home/${AI_USERNAME}/.ssh
curl -sSL https://gitlab.com/${AI_USERNAME}.keys >>  /home/${AI_USERNAME}/.ssh/authorized_keys || true
chown -R ${AI_USERNAME}. /home/${AI_USERNAME}

# install microcode
if grep -q Intel /proc/cpuinfo ; then
    pacman -Sy --noconfirm intel-ucode
    MICROCODE="initrd  /intel-ucode.img"
fi
if grep -q AMD /proc/cpuinfo ; then
    pacman -Sy --noconfirm amd-ucode
    MICROCODE="initrd  /amd-ucode.img"
fi

# Enable DHCPCD on interface
mkdir -p "/etc/systemd/system/multi-user.target.wants"
ln -sf "/usr/lib/systemd/system/dhcpcd@.service" "/etc/systemd/system/multi-user.target.wants/dhcpcd@${ETH}.service"
#systemctl enable dhcpcd@${ETH}

# install yay
sudo -iu pkgbuild git clone https://aur.archlinux.org/yay-bin.git /tmp/yay
sudo -iu pkgbuild bash -c "cd /tmp/yay && makepkg -cfsi --noconfirm"

pacman -Sy --noconfirm linux mkinitcpio dhcpcd

# Boot loader
bootctl install --path /boot
if [ -n "${CRYPTDEVICE}" ]; then
    UUID=$(blkid "${CRYPTDEVICE}" | tr ' ' '\n' | grep '^UUID' | tr -d '"')
    KERNELPARAMS="${KERNELPARAMS} cryptdevice=${UUID}:crypt:allow-discards"
    if [ -n "$VAULT_TOKEN" ]; then
        CSVPARAMS="-cryptdevice ${UUID} -cryptmapper crypt -cryptoptions allow-discards -token ${VAULT_TOKEN} -vault ${VAULT_ADDR}"
        cat > /etc/cryptsetup-vault <<EOF
VAULT_ADDR=${VAULT_ADDR}
VAULT_TOKEN=${VAULT_TOKEN}
TIMEOUT=240
EOF
        chmod 0600 /etc/cryptsetup-vault
        sudo -iu pkgbuild yay -Sy --cleanafter --noconfirm cryptsetup-vault
        echo -n "${CRYPTPW}" | cryptsetup-vault -setkey ${CSVPARAMS}
        cryptsetup-vault -test ${CSVPARAMS}
        sed -i 's;^HOOKS=.*;HOOKS=(base udev autodetect keyboard keymap consolefont modconf block netconf cryptsetupvault encrypt filesystems fsck);g' /etc/mkinitcpio.conf
        ORIG_ETH=$(dmesg | awk "\$0~/${ETH}: renamed from/ {print \$NF}")
        KERNELPARAMS="${KERNELPARAMS} ip=:::::${ORIG_ETH}:dhcp"
    else
        sed -i 's;^HOOKS=.*;HOOKS=(base udev autodetect keyboard keymap consolefont modconf block netconf encrypt filesystems fsck);g' /etc/mkinitcpio.conf
    fi
    mkinitcpio -p linux
fi
KERNELPARAMS="${KERNELPARAMS} root=${ROOTDEVICE}"

# Remove temporary user
userdel -r pkgbuild || true
groupdel pkgbuild || true

# Install and enable SSH
pacman -Sy --noconfirm openssh
systemctl enable sshd

# Configure bootloader
cat > /boot/loader/entries/ArchLinux.conf <<EOF
title   Arch Linux
linux   /vmlinuz-linux
$MICROCODE
initrd  /initramfs-linux.img
options ${KERNELPARAMS}
EOF

if [ -n "$GENCRYPTPW" ]; then
    echo "Generated password for FDE:"
    echo ""
    echo "${CRYPTPW}"
    echo ""
    echo "MAKE A BACKUP OF THIS"
fi

echo "Install OK"